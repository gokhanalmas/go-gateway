import { Moment } from 'moment';

export const enum TodoStatus {
  COMPLETED = 'COMPLETED',
  PENDING = 'PENDING',
  CANCELLED = 'CANCELLED'
}

export interface ITodo {
  id?: number;
  text?: string;
  done?: boolean;
  status?: TodoStatus;
  createdDate?: Moment;
}

export const defaultValue: Readonly<ITodo> = {
  done: false
};
